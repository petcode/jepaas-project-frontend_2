/**
 * 自定义功能树形面板
 */
Ext.define("PRO.demo.view.WestView", {
	/**
	 * 继承平台的树形面板
	 * @type String
	 */
	extend: 'JE.core.view.TreeView',
	/**
	 * 声明面板的xtype
	 * 确保全局唯一，widget.是系统必带的前缀
	 * 此类xtype的引用为：test.westview
	 * @type String
	 */
	alias: 'widget.demo.westview',
	multiSelect : false,//多选
	searchBar:true,//查询工具条
	useTools:false,//不启用工具条
	rootVisible : false,//不显示根节点
	onlyItem:true,//请求系统数据时的参数，只需要子项。默认true，以数组形式返回，false时，以根节点对象形式返回
    initComponent: function(){
    	var me = this;
    	/*
    		三种获取树形数据的方法
    		1.使用平台的字典项
	    		me.queryItems = [{title:'字典项的标题',field:'过滤的字段编码',configInfo:'字典编码'},可以配置多个...];
	    		me.onlyItem = false;
    		2.获取自定功能的数据
	    		me.url = '/je/dynaAction!getTree.action'||'自己的链接';
	    		me.onlyItem = true;
    			me.params = {tableCode:'表名',whereSql:'过滤条件',orderSql:'排序条件'};
    		3.直接设置树形的root节点数据
    			me.rootData = {id:'ROOT',text:'ROOT',children:[]}
    	*/
    	me.queryItems = [{title:'性别',field:'GENDER',configInfo:'JE_SEX'}];
	    me.onlyItem = false;
    	
        me.callParent(arguments);
        
    }
});