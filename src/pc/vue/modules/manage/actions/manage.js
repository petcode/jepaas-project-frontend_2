/*
 * @Descripttion: 描述
 * @version: V1.0.0
 * @Author: Shuangshuang Song
 * @LastEditors: Shuangshuang Song
 * @Date: 2020-05-23 12:34:52
 * @LastEditTime: 2020-06-15 11:01:45
 */
import fetch from '../../../static/js/common';

/**
 * 获取公司信息
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchCompanyInfo(param) {
  return fetch('/je/product/crm/customerPortrit/customerInfo', {}, {
    type: 'post',
    contentType: 'application/x-www-form-urlencoded',
    data: param,
    isFormSubmit: true,
  })
    .then(data => data)
    .catch();
}
/**
 * 获取公司信息-new
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchCompanyInfoNew(param) {
  // /je/dbsql/dbsql/selectList
  return fetch('/je/getInfoById', {}, {
    type: 'post',
    contentType: 'application/x-www-form-urlencoded',
    data: param,
    isFormSubmit: true,
  })
    .then(data => data)
    .catch();
}


/**
 * 获取组织信息统计信息
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchLoadUser(param) {
  return fetch('/je/rbac/rbac/loadUserCount', {}, {
    type: 'post',
    contentType: 'application/x-www-form-urlencoded',
    data: param,
    isFormSubmit: true,
  })
    .then(data => data)
    .catch();
}

/**
 * 获取活跃度信息统计信息
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchDept(param) {
  return fetch('/je/rbac/rbac/loadDeptUserCount', {}, {
    type: 'post',
    contentType: 'application/x-www-form-urlencoded',
    data: param,
    isFormSubmit: true,
  })
    .then(data => data)
    .catch();
}

/**
 * 登录信息
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchLoginCount(param) {
  return fetch('/je/rbac/rbac/loadLoginCount', {}, {
    type: 'post',
    contentType: 'application/x-www-form-urlencoded',
    data: param,
    isFormSubmit: true,
  })
    .then(data => data)
    .catch();
}

/**
 * 登录日志
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchLoginLog(param) {
  return fetch('/je/rbac/rbac/loadLoginLog', {}, {
    type: 'post',
    contentType: 'application/x-www-form-urlencoded',
    data: param,
    isFormSubmit: true,
  })
    .then(data => data)
    .catch();
}
