const gulp = require('gulp');// 打包工具
const chalk = require('chalk');// 控制台输出颜色
const path = require('path');// 路径
const fs = require('fs');// 路径
const uglify = require('gulp-uglify');// 压缩混淆js
const minifyCss = require('gulp-minify-css');// 压缩混淆css
const rev = require('gulp-rev');//
const revCollector = require('gulp-rev-collector');//
const filter = require('gulp-filter');// 过滤器
const mergeStream = require('merge-stream');// 合并任务流
const babel = require('gulp-babel');
const watch = require('gulp-watch');
const util = require('./../webpack/util');

const { config } = util;

const jsFilter = filter(['**/*.js'], { restore: true });
const cssFilter = filter(['**/*.css'], { restore: true });
const proFolder = 'pro';
// 构建ext项目
gulp.task('build-ext', () => {
  util.consoleInfo('开始构建Ext项目');
  let folder = ['src/pc/ext/**/*'];
  // 打包包含的入口文件
  if (config.entry && config.entry.length > 0) {
    folder = [];
    config.entry.forEach((entry) => {
      folder.push(`src/pc/ext/${entry}/**/*`);
    });
  }
  return gulp.src(folder, { base: 'src/pc/ext' })
    .pipe(jsFilter)
    // .pipe(babel({
    //     "plugins": ["transform-remove-strict-mode"]
    // }))
    .pipe(uglify())
    .on('error', (e) => {
      console.log(e);
    })
    .pipe(jsFilter.restore)
    .pipe(cssFilter)
    .pipe(minifyCss())
    .pipe(cssFilter.restore)
    .pipe(rev())
    .pipe(gulp.dest(`dist/${proFolder}`))
    .on('end', () => {
      console.log(chalk.gray('<<<<<< Ext 构建完成 '));
    });
});
// 监听
gulp.task('watch', () => {
  console.log(chalk.green('开启 src/pc/ext/**/* 文件监听'));
  return watch('./src/pc/ext/**/*', () => {
    util.hashMap();
  });
});
// 资源文件
gulp.task('build-resourse', () => {
  util.consoleInfo('开始构建resourse资源');
  const base = `config/resourse/project/${config.project}`;
  const holiday = `config/resourse/holiday/${config.holiday}`;
  const folder = [`${base}/**/*`];

  // 处理登录图片
  const loginCfg = config.loginConfig;
  const imgs = [];
  const holidayImgs = [];
  config.loginImgs.forEach((attr) => {
    const img = loginCfg[attr];
    if (img && img.length) {
      folder.push(`!**/${img}`);
      imgs.push(`${base}/${img}`);
      holidayImgs.push(`${holiday}/${img}`);
    }
  });
  // hash后缀
  const resourse = gulp.src(folder)
    .pipe(rev())
    .pipe(gulp.dest('dist'))
    .pipe(rev.manifest('resourse.json'))
    .pipe(gulp.dest('dist'));
  // 配置的文件，不缓存
  const file = gulp.src(imgs, { base })
    .pipe(gulp.dest('dist'))
    .on('end', () => {
      // 节假日的资源文件
      if (config.holiday) {
        gulp.src(holidayImgs, { base: holiday })
          .pipe(gulp.dest('dist'));
      }
    });
  return mergeStream(resourse, file)
    .on('end', () => {
      console.log(chalk.gray('<<<<<< Resourse 构建完成 '));
    });
});

gulp.task('build-rev', () =>
  // 映射hash
  gulp.src(['dist/resourse.json', 'dist/static/**/*.css'])
    .pipe(revCollector())
    .pipe(gulp.dest('dist/static'))
    .on('end', () => {
      const file = path.resolve(__dirname, '../../../', 'dist/resourse.json');
      if (fs.existsSync(file)) {
        const hashMap = JSON.parse(fs.readFileSync(file));
        const css = 'static/project/override.css';
        const hashCss = hashMap[css];
        if (hashCss) {
          fs.renameSync(path.resolve(__dirname, '../../../dist', hashCss), path.resolve(__dirname, '../../../dist', css));
        }
        fs.unlinkSync(file);
      }
      // 构建hash-map.js
    }));
